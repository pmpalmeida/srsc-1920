package exceptions;

public class ConfigNotFoundException extends Exception {

    public ConfigNotFoundException() {
        super();
    }

    public ConfigNotFoundException(String msg) {
        super(String.format("No config found for group '%s'", msg));
    }
}
