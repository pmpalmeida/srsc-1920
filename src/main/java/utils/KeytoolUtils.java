package utils;

import javax.crypto.SecretKey;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyStore;

public class KeytoolUtils {

    private static final String KEYSTORE_NAME = "SMCPKeystore";
    private static final String SEPARATOR = "&#&#";
    private static final String SYM_KEY = "SKEY";
    private static final String MAC_KEY = "MAC";
    private static final String IV_PARAM = "IV";

    /**
     * Creates or retrives a KeyStore
     *
     * @param pw
     * @return
     * @throws Exception
     */
    public static KeyStore createKeyStore(String pw) throws Exception {
        return createKeyStore(KEYSTORE_NAME, pw);
    }

    /**
     * Creates or retrives a KeyStore
     *
     * @param pw
     * @return
     * @throws Exception
     */
    public static KeyStore createKeyStore(String name, String pw) throws Exception {
        File file = new File(name);

        final KeyStore keyStore = KeyStore.getInstance("JCEKS");
        if (file.exists()) {
            // .keystore file already exists => load it
            keyStore.load(new FileInputStream(file), pw.toCharArray());
        } else {
            // .keystore file not created yet => create it
            keyStore.load(null, null);
            keyStore.store(new FileOutputStream(KEYSTORE_NAME), pw.toCharArray());
        }

        return keyStore;
    }

    /**
     * Retrieves the stored symmetric key
     *
     * @param keyStore
     * @param entryId
     * @param pwd
     * @return
     * @throws Exception
     */
    public static SecretKey getSKey(KeyStore keyStore, String entryId, String pwd) throws Exception {
        return getKey(keyStore, entryId + SEPARATOR + SYM_KEY, pwd);
    }

    /**
     * Retrieves the stored mac key
     *
     * @param keyStore
     * @param entryId
     * @param pwd
     * @return
     * @throws Exception
     */
    public static SecretKey getMACKey(KeyStore keyStore, String entryId, String pwd) throws Exception {
        return getKey(keyStore, entryId + SEPARATOR + MAC_KEY, pwd);
    }

    /**
     * Retrieves the stored iv param
     *
     * @param keyStore
     * @param entryId
     * @param pwd
     * @return
     * @throws Exception
     */
    public static SecretKey getIVParam(KeyStore keyStore, String entryId, String pwd) throws Exception {
        return getKey(keyStore, entryId + SEPARATOR + IV_PARAM, pwd);
    }

    /**
     * Retrieves the stored key
     *
     * @param keyStore
     * @param entryId
     * @param pwd
     * @return
     * @throws Exception
     */
    private static SecretKey getKey(KeyStore keyStore, String entryId, String pwd) throws Exception {
        KeyStore.PasswordProtection keyPassword = new KeyStore.PasswordProtection(pwd.toCharArray());
        KeyStore.Entry entry = keyStore.getEntry(entryId, keyPassword);
        return ((KeyStore.SecretKeyEntry) entry).getSecretKey();
    }

    /**
     * Stores the symmetric secret key
     *
     * @param keyStore
     * @param entryId
     * @param pwd
     * @param secretKey
     * @throws Exception
     */
    public static void storeSKey(KeyStore keyStore, String entryId, String pwd, SecretKey secretKey) throws Exception {
        storeKey(keyStore, entryId + SEPARATOR + SYM_KEY, pwd, secretKey);
    }

    /**
     * Stores the mac secret key
     *
     * @param keyStore
     * @param entryId
     * @param pwd
     * @param secretKey
     * @throws Exception
     */
    public static void storeMACKey(KeyStore keyStore, String entryId, String pwd, SecretKey secretKey) throws Exception {
        storeKey(keyStore, entryId + SEPARATOR + MAC_KEY, pwd, secretKey);
    }

    /**
     * Stores the iv secret param
     *
     * @param keyStore
     * @param entryId
     * @param pwd
     * @param secretKey
     * @throws Exception
     */
    public static void storeIVParam(KeyStore keyStore, String entryId, String pwd, SecretKey secretKey) throws Exception {
        storeKey(keyStore, entryId + SEPARATOR + IV_PARAM, pwd, secretKey);
    }

    /**
     * Stores the secret key
     *
     * @param keyStore
     * @param entryId
     * @param pwd
     * @param secretKey
     * @throws Exception
     */
    private static void storeKey(KeyStore keyStore, String entryId, String pwd, SecretKey secretKey) throws Exception {
        KeyStore.SecretKeyEntry keyStoreEntry = new KeyStore.SecretKeyEntry(secretKey);
        KeyStore.PasswordProtection keyPassword = new KeyStore.PasswordProtection(pwd.toCharArray());
        keyStore.setEntry(entryId, keyStoreEntry, keyPassword);
        keyStore.store(new FileOutputStream(KEYSTORE_NAME), pwd.toCharArray());
    }
}
