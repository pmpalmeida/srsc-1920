package utils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;

public class CipherUtils {
    public static byte[] encrypt(Cipher cipher, byte[] data, SecretKey sKey, byte[] iv) throws IllegalBlockSizeException {

        try {
            if (iv == null) {
                cipher.init(Cipher.ENCRYPT_MODE, sKey);
            } else {
                cipher.init(Cipher.ENCRYPT_MODE, sKey, new IvParameterSpec(iv));
            }
            return cipher.doFinal(data);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException | BadPaddingException e) {
            e.getMessage();
            System.exit(1);
        }
        return null;
    }

    public static byte[] decrypt(Cipher cipher, byte[] data, SecretKey sKey, byte[] iv) {
        try {
            if (iv == null) {
                cipher.init(Cipher.DECRYPT_MODE, sKey);
            } else {
                cipher.init(Cipher.DECRYPT_MODE, sKey, new IvParameterSpec(iv));
            }
            return cipher.doFinal(data, 0, data.length);
        } catch (Exception e) {
            e.getMessage();
            System.exit(1);
        }
        return null;
    }
}
