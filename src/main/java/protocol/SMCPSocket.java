package protocol;

import exceptions.ConfigNotFoundException;
import org.apache.commons.lang3.SerializationUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import utils.CipherUtils;
import utils.KeytoolUtils;
import utils.Utils;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SMCPSocket extends MulticastSocket {

    private static final String DEFAULT_PASSWORD = "fpj{chfse76:WzDQtq#']4@6/";
    private static final String S_ATTRIBUTES_ALG = "SHA-256";

    private InetAddress group;
    private String nickname;
    private String sessionName;
    private int seqNumber;

    private String sid;
    private int seaks;
    private String sea;
    private String mode;
    private String padding;
    private String intHash;
    private String mac;
    private int makks;

    private byte[] sAttributes;

    //Algorithm and sKey
    private KeyStore keyStore;
    private Mac hMac;
    private SecretKeySpec sKey;
    private Cipher cipher;
    private SecretKeySpec macKey;
    private Map<Integer, Integer> nonces;


    public SMCPSocket(String nickname, InetAddress group, int port) throws IOException {
        this(nickname, group, port, DEFAULT_PASSWORD);
    }

    public SMCPSocket(String nickname, InetAddress group, int port, String password) throws IOException {
        super(port);
        this.seqNumber = 0;
        this.nickname = nickname;
        this.init(group.getHostName() + ":" + port, password);
    }

    private void init(String ipPort, String password) {
        try {
            //Read config file
            getConfig(ipPort);

            String ciphersuite = getCiphersuite();

            //keyStore
            keyStore = KeytoolUtils.createKeyStore(password);

            //Symmetric Key
            handleSymKeyRetrieval(ciphersuite, ipPort, password);

            //Mac Key
            handleMacKeyRetrieval(ipPort, password);

            //CIPHER, MAC and Nonces
            cipher = Cipher.getInstance(ciphersuite, "BC");
            hMac = Mac.getInstance(mac, "BC");
            nonces = new HashMap<>();

        } catch (NoSuchPaddingException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getConfig(String ipPort) throws Exception {
        File config = new File("SMCP.conf");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(config);
        doc.getDocumentElement().normalize();

        Element n = null;
        Node e;

        NodeList nl = doc.getElementsByTagName("group");

        for (int i = 0; i < nl.getLength(); i++) {
            e = nl.item(i);
            n = (Element) e;
            if (((Element) e).getAttribute("id").equals(ipPort)) {
                break;
            }
            n = null;
        }

        if (n == null) {
            throw new ConfigNotFoundException(ipPort);
        }

        sid = ipPort;
        sessionName = n.getElementsByTagName("SID").item(0).getTextContent();
        sea = n.getElementsByTagName("SEA").item(0).getTextContent();
        seaks = Integer.parseInt(n.getElementsByTagName("SEAKS").item(0).getTextContent());
        mode = n.getElementsByTagName("MODE").item(0).getTextContent();
        padding = n.getElementsByTagName("PADDING").item(0).getTextContent();
        intHash = cleanIntHash(n.getElementsByTagName("INTHASH").item(0).getTextContent());
        mac = n.getElementsByTagName("MAC").item(0).getTextContent();
        makks = Integer.parseInt(n.getElementsByTagName("MAKKS").item(0).getTextContent());

        generateAttributes();
    }

    @Override
    public void send(DatagramPacket packet) throws IOException {

        int nonce = new SecureRandom().nextInt();
        //Get message
        byte[] msg = packet.getData();
        byte[] digest;

        ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
        DataOutputStream outStream = new DataOutputStream(byteOutStream);

        try {
            MessageDigest hash = MessageDigest.getInstance(intHash);
            hash.update(msg);
            digest = hash.digest();

            //FromPeerID
            outStream.writeLong(Long.parseLong(nickname, 36));
            //SequenceNumber
            outStream.writeInt(seqNumber++);
            //Nonce
            outStream.writeInt(nonce);
            //Message
            outStream.write(msg);
            //IntegrityControl
            outStream.write(digest);
            outStream.flush();

            byte[] securePayload = CipherUtils.encrypt(cipher, byteOutStream.toByteArray(), sKey, cipher.getIV());

            SMCPPacket secPacket = new SMCPPacket(sid, sAttributes, securePayload);
            byte[] b = SerializationUtils.serialize(secPacket);

            byte[] finalPacket = prepareFinalPacket(b);

            DatagramPacket p = new DatagramPacket(finalPacket, 0, finalPacket.length, packet.getSocketAddress());
            super.send(p);

        } catch (IllegalBlockSizeException e) {
            System.err.println(e.getMessage());
            System.err.println("Invalid padding...");
            System.exit(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receive(DatagramPacket packet) throws IOException {
        try {
            super.receive(packet);
            DataInputStream istream = new DataInputStream(
                    new ByteArrayInputStream(packet.getData(), 0, packet.getLength()));

            hMac.init(macKey);
            byte[] smcpPacket = new byte[packet.getLength() - hMac.getMacLength()];
            istream.read(smcpPacket, 0, smcpPacket.length);
            byte[] receivedCheck = new byte[hMac.getMacLength()];
            istream.read(receivedCheck, 0, receivedCheck.length);
            istream.close();

            if (Arrays.equals(receivedCheck, generateCheck(smcpPacket))) {

                SMCPPacket temp = (SMCPPacket) SerializationUtils.deserialize(smcpPacket);

                //Test Session Attributes Integrity
                if (Arrays.equals(this.sAttributes, temp.getsAttributes())) {

                    byte[] plaintext = CipherUtils.decrypt(cipher, temp.getPayload(), sKey, cipher.getIV());
                    istream = new DataInputStream(
                            new ByteArrayInputStream(plaintext, 0, plaintext.length));

                    //FromPeerID
                    long userID = istream.readLong();
                    //SequenceNumber
                    int seqN = istream.readInt();
                    //nonce
                    int nonce = istream.readInt();
                    //TODO - Dynamic MessageDigest size
                    byte[] message = new byte[temp.getPayloadSize() - 32];
                    istream.read(message, 0, message.length);
                    istream.close();

                    if (!nonces.containsKey(nonce)) {
                        nonces.put(nonce, nonce);
                        packet.setData(message);
                    } else {
                        throw new Exception("Nonce repeated. Message already received!");
                    }
                } else {
                    throw new Exception("Session Attributes Integrity check failed.");
                }
            } else {
                throw new Exception("FastSecurePayloadCheck failed.");
            }
        } catch (SocketTimeoutException ignored) {
        } catch (Exception e) {
            System.err.println("WARNING: " + e.getMessage());
        }
    }

    private void generateAttributes() {
        try {
            ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
            DataOutputStream outStream = new DataOutputStream(byteOutStream);

            outStream.write(Utils.toByteArray(sid));
            outStream.write(Utils.toByteArray(sessionName));
            outStream.write(Utils.toByteArray(sea));
            outStream.write(Utils.toByteArray(mode));
            outStream.write(Utils.toByteArray(padding));
            outStream.write(Utils.toByteArray(intHash));
            outStream.write(Utils.toByteArray(mac));

            MessageDigest md = MessageDigest.getInstance(intHash);
            sAttributes = md.digest();
        } catch (Exception e) {
            //TODO
        }
    }

    private byte[] generateCheck(byte[] data) {
        try {
            hMac.init(macKey);
            return hMac.doFinal(data);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] prepareFinalPacket(byte[] smcpPacket) {
        try {
            ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
            DataOutputStream outStream = new DataOutputStream(byteOutStream);

            outStream.write(smcpPacket);
            outStream.write(generateCheck(smcpPacket));
            outStream.flush();
            return byteOutStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void handleSymKeyRetrieval(String ciphersuite, String ipPort, String password) throws Exception {
        try {
            sKey = new SecretKeySpec(KeytoolUtils.getSKey(keyStore, ipPort, password).getEncoded(), ciphersuite);
        } catch (NullPointerException e) {
            KeyGenerator keyGen = KeyGenerator.getInstance(sea);
            keyGen.init(seaks);
            SecretKey secretKey = keyGen.generateKey();
            sKey = new SecretKeySpec(secretKey.getEncoded(), ciphersuite);
            KeytoolUtils.storeSKey(keyStore, ipPort, password, secretKey);
            secretKey = null;
        }
    }

    private void handleMacKeyRetrieval(String ipPort, String password) throws Exception {
        try {
            macKey = new SecretKeySpec(KeytoolUtils.getMACKey(keyStore, ipPort, password).getEncoded(), mac);
        } catch (NullPointerException e) {
            KeyGenerator keyGen = KeyGenerator.getInstance(mac);
            keyGen.init(makks);
            SecretKey secretKey = keyGen.generateKey();
            macKey = new SecretKeySpec(secretKey.getEncoded(), mac);
            KeytoolUtils.storeMACKey(keyStore, ipPort, password, secretKey);
            secretKey = null;
        }
    }

    private String getCiphersuite() {
        return String.format("%s/%s/%s", sea, mode, padding);
    }

    private String cleanIntHash(String inthash) {
        if (!inthash.contains("-") && !inthash.contains("SHAKE")) {
            if (inthash.startsWith("SHA")) {
                return "SHA-" + inthash.substring(3);
            }
        }
        return inthash;
    }
}
