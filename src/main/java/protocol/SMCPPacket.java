package protocol;

import java.io.Serializable;

public class SMCPPacket implements Serializable {

    private static final byte VID = 0x01;
    private static final byte MSG_TYPE = 0x01;

    private String sID;
    private byte[] sAttributes;
    private int payloadSize;
    private byte[] payload;


    public SMCPPacket(String sID, byte[] sAttributes, byte[] payload) {
        this.sID = sID;
        this.sAttributes = sAttributes;
        this.payload = payload;
        this.payloadSize = payload.length;
    }

    public SMCPPacket(String sID, byte[] payload) {
        this.sID = sID;
        this.payload = payload;
        this.payloadSize = payload.length;
    }

    public static byte getVID() {
        return VID;
    }

    public static byte getMsgType() {
        return MSG_TYPE;
    }

    public String getsID() {
        return sID;
    }

    public byte[] getsAttributes() {
        return sAttributes;
    }

    public int getPayloadSize() {
        return payloadSize;
    }

    public byte[] getPayload() {
        return payload;
    }
}
